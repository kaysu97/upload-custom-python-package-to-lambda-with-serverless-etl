from bs4 import BeautifulSoup
import datetime
import urllib.request
import os
import lxml
import pandas as pd
import numpy as np
import boto3
import logging
from botocore.exceptions import ClientError

CITY = []
DISTRICT = []
GEOCODE = []
DAY = []
TIME = []
T = []
TD = []
MaT = []
MiT = []
MaAT = []
MiAT = []
MaCI = []
MiCI = []
P12 = []
WS = []
RH = []
WD = []
WX = []

WT = []
BF = []

LAT = []
LON = []

detail = []

x = datetime.datetime.now() #現在時間
year = str(x.year) 
month = str(x.month) 
date = str(x.day) 

today = str(datetime.date.today())
s3 = boto3.resource('s3')
CSVBucketName = "<YOUR BUCKET NAME FOR Storing your transformed csv data>String"
XMLBucketName = "<YOUR BUCKET NAME FOR Storing your original xml data and this script with all packages>String"

def lambda_handler(event, context):
    try:
        # 下載xml檔案到S3的XML bucket
        res = "https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/F-D0047-091?Authorization=CWB-D2B5FD2D-AC71-4DDE-8793-8C0E4AD7482C&format=xml"
        r = urllib.request.urlretrieve(res,'/tmp/weather.xml')
        s3.Object(XMLBucketName, 'weatherXML/weather' + today+'.xml').upload_file('/tmp/weather.xml')
        # 將xml 檔案轉成csv
        save_name = frameData(res)
        #將轉成csv的檔案存到CSV bucket
        Path = save_name
        Key = "year="+year+"/"+"month="+ month+"/" +"date="+ date +"/"+"weather" + today + ".csv"
        s3.Object(CSVBucketName, Key).upload_file(Path)
    except botocore.exceptions.ClientError as e:
        logging.error(e)
        return False
    return True

#將xml flatten並轉檔成csv
def frameData(res):
    r=urllib.request.urlopen(res)
    weather = r.read().decode('utf-8')

    soup = BeautifulSoup(weather,"xml")
    city = soup.locationsName.text
    a = soup.find_all("location")
    for i in range(0,len(a)):
        location = a[i]
        district = location.locationName.text
        geocode = location.geocode.text
        lat = location.lat.text
        lon = location.lon.text
        weather = location.find_all("weatherElement")
        # time 
        time = weather[0].find_all("time")

        for j in range(0,len(time)):
            start = time[j].startTime.text.split("T")
            DAY.append(start[0])
            time_1 = start[1].split("+")
            TIME.append(time_1[0])
            DISTRICT.append(district)
            GEOCODE.append(geocode)
            LAT.append(lat)
            LON.append(lon)
            #平均溫度
            t = weather[0].find_all("value")[j].text
            T.append(t)

            #平均露點溫度
            td = weather[1].find_all("value")[j].text
            TD.append(td)
            #平均相對溼度
            rh = weather[2].find_all("value")[j].text
            RH.append(rh)
            #最高溫度
            mat  = weather[3].find_all("value")[j].text
            MaT.append(mat)
            #最低溫度
            mit = weather[4].find_all("value")[j].text
            MiT.append(mit)
            #最高體感溫度
            maAT = weather[5].find_all("value")[j].text
            MaAT.append(maAT)
            #最低體感溫度
            miAT = weather[6].find_all("value")[j].text
            MiAT.append(miAT)
            #最大舒適度指數
            maCI = weather[7].find_all("value")[j].text
            MaCI.append(maCI)
            #最小舒適度指數
            miCI = weather[8].find_all("value")[j].text
            MiCI.append(miCI)
            #12小時降雨機率
            p12 = weather[9].find_all("value")[j].text
            P12.append(p12)
            #風向
            wd = weather[10].find_all("value")[j].text
            WD.append(wd)
            #最大風速WS,蒲伏風級BF
            ws = weather[11].find_all("time")
            tt = ws[j].find_all("value")
            for k  in range(0,len(tt)-1,2):
                WS.append(tt[k].text)
                BF.append(tt[k+1].text)

            # 天氣現象WX
            wx = weather[12].find_all("time")
            qq = wx[j].find_all("value")
            for w in range(0,len(qq)-1,2):
                WX.append(qq[w].text)
                
            
            #天氣預報綜合描述
            wt = weather[14].find_all("value")[j].text
            WT.append(wt)

    data = {"DISTRICT":DISTRICT,"GEOCODE":GEOCODE,"緯度":LAT,"經度":LON,"DAY" : DAY,"TIME" : TIME,"平均溫度":T,"平均露點溫度" : TD,
            "平均相對溼度":RH,"風向" : WD,"最大風速" : WS,"蒲福風級":BF,"天氣現象": WX,"12小時降雨機率" :P12,"最高溫度":MaT,
            "最低溫度":MiT,"最高體感溫度":MaAT,"最低體感溫度":MiAT,"最大舒適度指數":MaCI,"最小舒適度指數":MiCI,"天氣預報綜合描述":WT}
    #建成用pandas套件定義的dataframe
    df = pd.DataFrame(data,columns=["DISTRICT","GEOCODE","緯度","經度","DAY","TIME","平均溫度","平均露點溫度","平均相對溼度"
                                    ,"風向","風速","蒲福風級","天氣現象","12小時降雨機率","最高溫度","最低溫度",
                                    "最高體感溫度","最低體感溫度","最大舒適度指數","最小舒適度指數","天氣預報綜合描述"])

    today = str(datetime.date.today())
    file_path = '/tmp'
    save_name = "weather" + today + ".csv"
    save_name = file_path + "/" + save_name
    df.to_csv(save_name,index=False,encoding="utf_8_sig") #轉檔
   
    return save_name





