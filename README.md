# Upload Custom Python Packages To Lambda with Serverless ETL 

<img src="https://i.imgur.com/U11yVvY.png" width=800 height=300 />

## Scenario
ETL is a very important part in a data journey. We can't make the data useful without it to downstream application , such as building the model , analysing and visualizing the data .
In this lab, we'll use the open data of [Central Weather Bureau](https://opendata.cwb.gov.tw/dist/opendata-swagger.html) for the weather forcast in a week ,and its datastore ID is 'F-D0047-091'. 
Through our observation, we can know it is a nested data which is not a good structure to be processed and cleaned. Thus, we have to flatten and transform it from xml to csv format with python BeautifulSoup package. 

![](https://i.imgur.com/K8SZgEt.png)

In this lab, we'll put python script in an s3 bucket and use lambda to run the code. Because this data will update every day, we'll use CloudWatch to trigger this Lambda function to run the code regularlly.
After that, the transformed data will be saved in an s3 bucket. At the same time, it'll trigger lambda to call Glue Crawler. 
At last, we'll use Athena CTAS query to create a new table with parquet format which can compress the data size and increase query speed.

AWS CloudFormation is a crucial service to anyone who wants to make an architect for the project. So in this case, we'll try to practice designing a simple template to create buckets which is used for storing your script , csv format data and parquet format data.
* NOTE: In this lab, we'll use EC2 for installing python package because the environment in the EC2 is as the same as in Lambda. 
## Prerequisites
* The workshop’s region should be in ‘N.Virginia’.
* Windows user can download Bitvise SSH Client:
    If you don’t have the Bitvise SSH Client installed on your machine, you can download and then launch it from here:
    https://www.bitvise.com/ssh-client-download

    And the link below will teach you how to use Bitvise to connect to EC2:
    
    https://www.youtube.com/watch?v=cF-X06hpmUs
    
    * NOTE: Bitvise has FTP function. So it is easier for uploading the file to EC2. If you have another way to upload the file to EC2, you don't need to use Bitvise.
* Download s3Bucket-template and lambda_function.py in this lab. 
* In the lambda_function.py file, modify the bucket names which are for storing original xml data and transformed csv data. They must be the same name as you make when you use s3Bucket-template in CloudFormation.
    ![](https://i.imgur.com/2R9RBti.png)

* Remember tag your name when you create anything to make sure which one belonged to you. Or just remember the ID.
* Download Tableau Desktop for 14 days free trial:
 https://www.tableau.com/zh-tw/products/trial
* Setup AWS Athena Driver for Tableau Desktop: 
Click below link to download and install the latest Java version
https://www.java.com/en/download
* Download JDBC driver (.jar file) that matches your version of the JDK and the JDBC data standards:
https://docs.aws.amazon.com/athena/latest/ug/connect-with-jdbc.html
For Mac users, create the JDBC directory and copy the downloaded .jar file to the /Library/JDBC directory.
For Windows users, migrate .jar file to C:\Program Files\Tableau\Drivers.
## Lab tutorial
### Step 0: Create IAM role for Lambda and Glue
1. Create a role for Lambda.
    * Choose Lambda service to use this role.
    * Choose AmazonS3FullAccess role policy.
    * After creating , add the inline policy below.
        ```
        {
            "Version": "2012-10-17",
            "Statement": 
            [
                {
                    "Sid": "VisualEditor0",
                    "Effect": "Allow",
                    "Action": [
                        "athena:*",
                        "glue:*"
                    ],
                    "Resource": "*"
                }
            ]
        } 
        ```
2. Create a role for Glue.
    * Choose Glue service to use this role.
    * Choose AmazonS3FullAccess、AWSGlueConsoleFullAccess role policy.
    * After creating , add the inline policy below.
        ```
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "logs:CreateLogGroup",
                        "logs:CreateLogStream",
                        "logs:PutLogEvents",
                        "logs:DescribeLogStreams"
                    ],
                    "Resource": [
                        "arn:aws:logs:*:*:*"
                    ]
                }
            ]
        }
        ```
### Step 1: Use CloudFormation to create S3 bucket 
1. Use s3Bucket-template file in this lab to create a stack.
2. Choose upload the template.
    ![](https://i.imgur.com/E4wQctj.png)

3. Input the bucket name you want.
    ![](https://i.imgur.com/fN90UQY.png)

4. Permission Part: choose Cloudformation-admin.
### Step 2: Upload python script to Lambda
1. Build up an EC2 with Linux and use Bitvise to connect to it.
    * NOTE: Remember to attach AllowEC2Admin role on EC2
2. Run the code below:
    ```
    # We'll use python 3 , so if you don't have , please install one
    sudo pip install python36
    # Build up a python3 virtual environment which is named v-env
    python3 -m venv v-env
    # Activate the environment. You'll enter into (v-env) environment.
    source v-env/bin/activate
    ```
    ![](https://i.imgur.com/47HHDnC.png)
3. Then you can install the package you want. In this lab, you'll use the package below:
    ```
    pip install bs4
    pip install lxml
    pip install pandas
    pip install numpy
    ```
4. Use Bitvise FTP into v-env/lib/python3.6/site-packages and upload lambda_function.py file in this lab to the EC2.
5. Compress all files in the "site-package" file. And upload your zip file to the bucket you created before to store xml data. 
    ```
    cd v-env/lib/python3.6/site-packages
    zip -r9 lambda_function.zip *
    # Upload zip file to s3
    aws s3 cp lambda_function.zip s3://<YOUR BUCKET NAME>/<YOUR KEY>
    ```
    ![](https://i.imgur.com/bpQJnlz.png)
 
6. Go to Lambda console page and create a function.
    * Choose Python 3.6 for Runtime.
    * Choose the role you just created in step 0.
    ![](https://i.imgur.com/DHL9dmS.png)
7. Editing some Lambda function settings
    * Adjust memory to 256 MB and set Timeout to 5 min
        ![](https://i.imgur.com/c7t6Yni.png)
    * After finishing all settings, click save and create a Test. 
        ![](https://i.imgur.com/EKpuRS3.png)

    * If your test is successful, you'll see successful log.
    * Go to the bucket storing your csv data. You can see your data is partitioned by year , month and date. Partition can make connection to different table and make query faster.
        ![](https://i.imgur.com/EUa0RMS.png)
        ![](https://i.imgur.com/pP0vhQt.png)
 
8. Add CloudWatch Schedule to your Lambda function for updating your data set
    * We want CloudWatch to trigger lambda at 11 am every day. 
    * Fill in Cron expression <0 3 ? * * *>. If you want to change updating time,see https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html 
        ![](https://i.imgur.com/50BoI9V.png)
    * If you created successfully, you'll see your lambda function has CloudWatch trigger.
        ![](https://i.imgur.com/Qf3kbn8.png)
### Step 3: Make Glue Crawler crawl CSV data in the s3 bucket
1. Create a Glue Crawler 
    * Choose the IAM role you created in step 0.
        ![](https://i.imgur.com/SFkuDZ8.png)
    * If you haven't created a database, click add database.
        ![](https://i.imgur.com/s7gwUkh.png)

    * For all the other options, just maintain default value. 
2. Create a new Lambda function to trigger Glue Crawler
    * Choose the IAM role you created in step 0.
    * After creating the function,add the code below to the Lambda function
        ```
        import json
        import boto3

        client = boto3.client('glue')

        def lambda_handler(event, context):
            # TODO implement
            response = client.start_crawler(
                Name=<YOUR CRAWLER NAME>
        )
        ```
        ![](https://i.imgur.com/2y7fnEU.png)
    * Choose Add trigger this lambda function
        * Choose the bucket you created for storing CSV data and Event type is "All object create event"
        * If you add it successfully , you'll see the piceture below.
    * If you add S3 trigger successfully, as long as your csv bucket has copy,put or post event,Glue will crawl data from that bucket automatically.
        ![](https://i.imgur.com/uLkZi4w.png)
### Step 4: Use Athena CTAS to create new table and transform data format to parquet
1. Use Athena to query your csv data
    * If your Glue Crawler is successful, you can see your data in Athena.
    * In the left navigation pane, you'll see (Partition) mark. It means this table originates from different tables , and Glue turned the classified file into a new column to merge the different tables in S3. (We mentioned it in point 7 in step 2 before.)
    * In this case, the new columns are year,month and date.
    ![](https://i.imgur.com/rFuSLhg.png)
2. Use CTAS to create new table and transform data to parquet format
    * Click the Settings button and type the bucket you created for Athena parquet format data.
    * Click Create and choose "Create table from query"
        ![](https://i.imgur.com/R47S8da.png)
    * Fill the input forms and the "Output location" is what you just use in Settings button.
    * Output data format: choose Parquet.
        ![](https://i.imgur.com/bPIS4Ae.png)
    * Click next,copy code below to the query form. To learn more , see https://docs.aws.amazon.com/athena/latest/ug/ctas-examples.html
        ```
        CREATE TABLE <YOUR DATABASE>.<YOUR NEW TABLE>
        WITH (
          format='PARQUET',
          partitioned_by = ARRAY['year','month','date']
        ) AS
        SELECT * FROM "<YOUR DATABASE>"."<YOUR TABLE>" 
        ```        
* If you created successfully, you can see Athena will partition your new table automatically by today's year,month and date.
    ![](https://i.imgur.com/ifJVmZf.png)
* With `partitioned_by = ARRAY['year','month','date']` in the query code, you'll find your different tables are separated by column year,month and date.
    ![](https://i.imgur.com/jjCIDUs.png)
3. After transforming the data, if you still want to do some ETL for this data, you can crawl the data back to Glue.
### Step 5: Connect S3 data using Athena with Tableau Desktop
1. In the Tableau Desktop, click Amazon Athena from the left navigation pane to connect.
![](https://i.imgur.com/czoV74a.png)
* In the server connection window, configure:
 Server:athena.ap-us-east-1.amazonaws.com
 Port:443
 S3 Staging Directory: click Athena Settings to get the query result location, it looks like "s3://aws-athena-query-results-(numbers)-ap-northeast-2/"
Access Key ID and Secret Access Key: created from your IAM users

2. After sign in, select your database in Catalog and drag the table which you want to use.
![](https://i.imgur.com/HThtWCT.png)
At the bottom of your screen, click the new work sheet, it will navigate you to the design pane.


## Conclusion
Congratulations! 😊 In this post, we explored:
* Use custom python packages on Lambda.
* Use CloudWatch schedule and S3 to trigger Lambda run the codes.
* Use Lambda function with boto3 to call Glue.
* Create Glue Crawler to crawl the data from S3
* Create a new table and transform the data with Athena CTAS from Glue Crawler Catalog.
* Connect S3 data using Glue with Athena connector in Tableau Desktop.
 

